/**
 * IGNORE THIS FILE
 */

import {
  AcademicPartnerIntegration,
  IIntegrationBase,
  IIntegrationConfigBase,
} from '../../../academic-partner/types/integrations.types';

export interface IDirectIntegrationIntegration extends IIntegrationBase {
  type: AcademicPartnerIntegration.SEND_LEAD_DIRECT_INTEGRATION;
  config: IIntegrationConfigBase; //IDirectIntegrationIntegrationConfigs;
}

// interface IDirectIntegrationIntegrationConfigs
//   extends IIntegrationsConfigBase {}
