import _ from 'lodash';
import { Injectable } from '@nestjs/common';
import { ILeadsWithMetadata } from '../types/CSVFlow.types';

@Injectable()
export class FlowUtils {
  parseValue(value: unknown, applyFunction: string): string {
    switch (applyFunction) {
      case 'dateFormatter':
        return this.dateFormatter(value);

      case 'normalizeDirectBill':
        return this.normalizeDirectBill(value);

      case 'parseCountry':
        return this.normalizeDirectBill(value);

      case 'parseEligibilityStatus':
      case 'isEligible':
      default:
        return String(value);
    }
  }

  normalizeDirectBill(hasDirectBill) {
    return hasDirectBill === false ? 'N' : 'Y';
  }

  parseCountry(country) {
    return country.length == 3 ? country.toUpperCase() : 'USA';
  }

  dateFormatter(value) {
    return new Intl.DateTimeFormat('en-US', {
      timeZone: 'UTC',
      month: '2-digit',
      day: '2-digit',
      year: 'numeric',
    }).format(value);
  }

  // Mostly lodash functions
  applyMethod(
    name: string,
    args: (string | number | string[] | number[])[],
    leadsToIngest: ILeadsWithMetadata[],
  ): ILeadsWithMetadata[] {
    switch (name) {
      case 'orderBy':
        return _.orderBy(leadsToIngest, ...args);

      case 'uniqWith':
        return _.uniqWith(leadsToIngest, (first, second) =>
          args.every((key) =>
            typeof key !== 'string'
              ? false
              : _.isEqual(first[key], second[key]),
          ),
        );

      default:
        return leadsToIngest;
    }
  }
}
