import { Injectable, MethodNotAllowedException } from '@nestjs/common';
import { SendLeadsCsvRepository } from '../send-leads-csv.repository';
import { ICSVFlowResult, ILeadsTimeframe } from '../types/CSVFlow.types';
import {
  ISendLeadsCSVIntegrationConfigs,
  TimeFrameType,
} from '../types/SendLeadsCSV.integration.type';

@Injectable()
export class LeadTimeframe {
  constructor(protected readonly repository: SendLeadsCsvRepository) {}

  /** Determine how many periods will be executed depending on the lastTimeFram type and the last run this interaction was run for the AP.
   * @returns At least one period
   */
  getPeriods(
    academicPartnerId: string,
    config: ISendLeadsCSVIntegrationConfigs,
  ): ILeadsTimeframe[] {
    // CUMMULATIVE
    if (config.leadsTimeframe.type === TimeFrameType.CUMMULATIVE)
      // One run, all the leads
      return [undefined];

    // DELTAS
    // Most of the time this delta period of time is 1 day.
    if (config.leadsTimeframe.type === TimeFrameType.DELTA) {
      const lastSuccessFullRun =
        this.repository.getLastSuccessfulRun(academicPartnerId);

      return this.calculatePeriodsForDeltas(lastSuccessFullRun, config);
    }

    throw new MethodNotAllowedException({
      message: "leadsTimeframe.type wasn't matched",
      metadata: {
        academicPartnerId,
        config,
      },
    });
  }

  private calculatePeriodsForDeltas(
    lastSuccessFullRun: Date,
    config: ISendLeadsCSVIntegrationConfigs,
  ): ILeadsTimeframe[] {
    /* 
    STEPS:
    - Calculate the day before today (execution day)
    - considering the delta days (usually 1 day) calculate the number of periods that should be proccesed since the last successful run.
    - create the `from` and `to` payload for quering properly the leads based on the periods and the delta days
    */
    throw new Error('Method not implemented.');
  }
}
