import { Employee, Lead } from '../../../shared/coreapi/types/generic.types';
import { IEncriptation } from '../../../shared/encriptation/types/encriptation.types';
import { IAPCommunication } from '../../../shared/academic-partner-communication/types/academic-partner-communication.types';
import type {
  AcademicPartnerIntegration,
  IIntegrationBase,
  IIntegrationConfigBase,
} from '../../../academic-partner/types/integrations.types';

// We explicitly set one integration with its own configs, in this case the CREATE_LEAD_CSV integration
export interface ISendLeadsCSVIntegration extends IIntegrationBase {
  type: AcademicPartnerIntegration.CREATE_LEAD_CSV;
  config: ISendLeadsCSVIntegrationConfigs;
}

export interface ISendLeadsCSVIntegrationConfigs
  extends IIntegrationConfigBase {
  /** Selects the flow that this integration should use */
  flow: CreateLeadsCSVFlow;
  /** Gets leads Cummulative or Deltas */
  leadsTimeframe: ITimeFrame;
  /** Defines how this flow particulary will create the CSV file  */
  flowConfigurations: IFlowConfigurations;
  /** The way we will send the CSV file */
  communication: IAPCommunication;
}

export interface IFlowConfigurations {
  /** Allows append data to leads */
  fetchingData?: {
    /** includes employees as metadata */
    employees?: boolean;
    /** includes corporatePartners as metadata */
    corporatePartners?: boolean;
    /** includes profiles as metadata */
    profiles?: boolean;
    /** includes programs as metadata */
    programs?: boolean;
    /** includes programs as metadata */
    consents?: boolean;
  };
  /** How to filter groupped data to create the leads to update */
  filteringCritaria?: {
    /** filters that apply to leads */
    leads?: {
      /** `true`: don't check whether the lead have AGREED consent */
      omitConsent?: boolean;
      /** `true`: don't check whether the lead have instrideEmployeeId */
      omitInstrideEmployeeId?: boolean;
      /** overwrite root omitTestUsers */
      omitTestUsers?: boolean;
    };
    /** filters that apply to profiles */
    profiles?: {
      /** overwrite root omitTestUsers */
      omitTestUsers?: boolean;
      /** exclude recipients */
      omitRecipients?: boolean;
    };
    /** filters that apply to employees */
    employees?: {
      /** overwrite root omitTestUsers */
      omitTestUsers?: boolean;
    };
    /** filters that apply to corporate partners */
    corporatePartners?: {
      /** List of CP.id to omit processing */
      omitIds?: string[];
    };
  };
  /** defines how to select leads will be updated to SCHEDULED  */
  leadsToUpdate: ISelectLeads[];
  /** defines other leasd that will integrate te csv file  */
  otherLeadsToIngest?: ISelectLeads[];
  /** define how will be the content of the file */
  csvContent: ICSVContent;
}

export interface ISelectLeads {
  employees?: {
    eligibilityStatus?: ISelectLeadsFiltering<Employee['eligibilityStatus'][]>;
  };
  leads?: {
    leadStatus?: ISelectLeadsFiltering<Lead['leadStatus'][]>;
  };
  programs?: {
    credits?: ISelectLeadsFiltering<IProgramsCreditsFiltering>;
  };
  corporatePartners?: {
    /** holdLeads, mannualverfication, mannualverification2, etc */
    flags?: ISelectLeadsFiltering<string[]>;
  };
}

export interface ISelectLeadsFiltering<T> {
  include?: T;
  exclude?: T;
}

export interface IProgramsCreditsFiltering {
  gt?: number;
  gte?: number;
  eq?: number;
  lt?: number;
  lte?: number;
}

export interface ICSVContent {
  beforeColumnsSelection?: ICSVContentSteps[];
  columns: {
    order: number;
    columnName: string;
    sources?: string[];
    applyFunction?: string;
    fallback?: unknown;
    /** If present, takes priorities over the other methods */
    fixedValue?: unknown;
  }[];
}

export type ICSVContentSteps = IStepApplyFunction;

export interface IStepApplyFunction {
  order: number;
  method: IApplyFunction;
}

export interface IApplyFunction {
  name: string;
  args?: (string | number | string[] | number[])[];
}

// Defined Flows available -> Each flow will be instanciated from a different Class
export enum CreateLeadsCSVFlow {
  NORMAL_FLOW = 'NORMAL_FLOW',
  OTHER_FLOW = 'OTHER_FLOW',
}

// Cummulative vs deltas leads
type ITimeFrame = ITimeFrameCummulative | ITimeFrameDelta;

interface ITimeFrameBase {
  type: TimeFrameType;
}

export enum TimeFrameType {
  /** All leads will be fetched */
  CUMMULATIVE = 'CUMMULATIVE' /* DEFAULT */,
  /** Leads from a specific period of time will be taken */
  DELTA = 'DELTA',
}

interface ITimeFrameCummulative extends ITimeFrameBase {
  type: TimeFrameType.CUMMULATIVE;
}

interface ITimeFrameDelta extends ITimeFrameBase {
  type: TimeFrameType.DELTA;
  config: {
    /** Numbers of days that we should take leads since this workflow process is triggered. (positive integer)
     * Ex, today is 01/15, if days is 1 then we will be taken leads from 01/14 only
     * Ex, today is 01/15, if days is 2 then we will be taken leads from 01/13 to 01/14 */
    days: number;
    /** Assure no leads was missing to communicate to AP. Deltas leads require most of the time to send the CSV file for the period of time in which wasn't sent. This feature will prevent sending the CSV for every period that is missing.*/
    omitMissingPeriods?: boolean;
    /** Doesn't update the last successful run for DELTA leads  */
    omitUpdatingLastSuccessfulRun?: boolean;
  };
}
