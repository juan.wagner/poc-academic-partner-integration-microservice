import {
  CorporatePartner,
  Employee,
  Profile,
  Lead,
  Program,
  Consent,
} from '../../../shared/coreapi/types/generic.types';
import { ISendLeadsCSVIntegrationConfigs } from './SendLeadsCSV.integration.type';

export interface IExecutableFlow {
  exec(
    academicPartnerId: string,
    config: ISendLeadsCSVIntegrationConfigs,
    leadPeriod?: ILeadsTimeframe,
  ): ICSVFlowResult;
}

export type PickedValues = Record<string, string>;

export interface ICSVFlowResult {
  // CSV content
  CSVLeads: string;
  // Leads that shoul be updated becase they were notified to the AP
  leadsToUpdate: ILeadsWithMetadata[];
}

// Common basic data that we use to build any CSV file
export interface ILeadsWithMetadata {
  lead: Lead;
  metadata: {
    employee?: Employee;
    corporatePartner?: CorporatePartner;
    profile?: Profile;
    program?: Program;
    consent?: Consent;
    isRecipient?: boolean;
  };
}

export interface ILeadsTimeframe {
  from: Date;
  to: Date;
}
