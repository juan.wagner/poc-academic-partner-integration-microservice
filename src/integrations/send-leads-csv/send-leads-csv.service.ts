import { Injectable } from '@nestjs/common';
import { AcademicPartnerCommunicationService } from '../../shared/academic-partner-communication/academic-partner-communication.service';
import { CoreapiService } from '../../shared/coreapi/coreapi.service';
import { DefaultFlow } from './flows/DefaultFlow';
import { LeadTimeframe } from './utils/LeadTimeframe';
import { SendLeadsCsvRepository } from './send-leads-csv.repository';
import { ICSVFlowResult, IExecutableFlow } from './types/CSVFlow.types';
import {
  CreateLeadsCSVFlow,
  ISendLeadsCSVIntegration,
  TimeFrameType,
} from './types/SendLeadsCSV.integration.type';

@Injectable()
export class SendLeadsCsvService {
  constructor(
    private readonly coreAPIService: CoreapiService,
    private readonly APCommunicationService: AcademicPartnerCommunicationService,
    private readonly repository: SendLeadsCsvRepository,
    private readonly leadTimeframe: LeadTimeframe,
    // All flows used are called here
    private readonly defaultFlow: DefaultFlow,
  ) {}

  sendLeadsCSV(
    academicPartnerId: string,
    { config }: ISendLeadsCSVIntegration,
  ) {
    const results: ICSVFlowResult[] = [];

    // We use the config to detrermine with flow should we use
    const flow = this.getFlow(config.flow);

    // Get excecution iterations
    const leadPeriods = this.leadTimeframe.getPeriods(
      academicPartnerId,
      config,
    );

    for (const leadPeriod of leadPeriods) {
      results.push(flow.exec(academicPartnerId, config, leadPeriod));
    }

    if (!config.dryRun) {
      for (const { CSVLeads, leadsToUpdate } of results) {
        // we send the CSV file to the AP
        this.APCommunicationService.sendCSV(CSVLeads, config.communication);

        // if (!config.updateProcesedLeadsStatus)
        // we update the lead status to "SENT"
        this.coreAPIService.updateLeads(leadsToUpdate);
      }

      if (
        config.leadsTimeframe.type === TimeFrameType.DELTA &&
        config.leadsTimeframe.config.omitUpdatingLastSuccessfulRun !== true
      )
        this.repository.updateLastSuccessfulRun(academicPartnerId, new Date());
    }

    return results;
  }

  // Like a flow factory
  private getFlow(flow: CreateLeadsCSVFlow): IExecutableFlow {
    switch (flow) {
      case CreateLeadsCSVFlow.NORMAL_FLOW:
      default:
        return this.defaultFlow;
    }
  }
}
