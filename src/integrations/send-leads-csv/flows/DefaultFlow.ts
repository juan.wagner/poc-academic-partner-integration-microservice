import _ from 'lodash';
import { Injectable } from '@nestjs/common';
import { CoreapiService } from '../../../shared/coreapi/coreapi.service';
import { SendLeadsCsvRepository } from '../send-leads-csv.repository';
import { FlowUtils } from '../utils/FlowUtils';
import { AbstractCSVFlow } from './abstract/CSVFlow';
import { IExecutableFlow } from '../types/CSVFlow.types';

@Injectable()
export class DefaultFlow extends AbstractCSVFlow implements IExecutableFlow {
  constructor(
    protected readonly coreService: CoreapiService,
    protected readonly utilsService: FlowUtils,
    protected readonly repository: SendLeadsCsvRepository,
  ) {
    super();
  }
}
