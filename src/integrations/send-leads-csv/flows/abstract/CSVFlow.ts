import _ from 'lodash';
import { CoreapiService } from '../../../../shared/coreapi/coreapi.service';
import {
  CorporatePartner,
  Employee,
  Lead,
  LeadWithConsent,
  Profile,
  Program,
} from '../../../../shared/coreapi/types/generic.types';
import { SendLeadsCsvRepository } from '../../send-leads-csv.repository';
import {
  ICSVFlowResult,
  ILeadsTimeframe,
  ILeadsWithMetadata,
  PickedValues,
} from '../../types/CSVFlow.types';
import {
  IFlowConfigurations,
  ISendLeadsCSVIntegrationConfigs,
  ICSVContentSteps,
  ICSVContent,
} from '../../types/SendLeadsCSV.integration.type';
import { FlowUtils } from '../../utils/FlowUtils';

export abstract class AbstractCSVFlow {
  protected readonly coreService: CoreapiService;
  protected readonly utilsService: FlowUtils;
  protected readonly repository: SendLeadsCsvRepository;

  // Common building steps
  public exec(
    academicPartnerId: string,
    config: ISendLeadsCSVIntegrationConfigs,
    leadPeriod?: ILeadsTimeframe,
  ): ICSVFlowResult {
    // Get all leads with their metadata
    const fetchedData = this.getCommonData(
      academicPartnerId,
      config,
      leadPeriod,
    );

    const filteredLeads = this.filterData(fetchedData, config);

    // Files that will be updated to SCHEDULED
    const leadsToUpdate = this.getLeadsToUpdate(
      filteredLeads,
      config.flowConfigurations.leadsToUpdate,
    );

    // Other leads that should be ingested in the getCSVContent but don't be updated
    const otherLeadsToIngest = this.getOtherLeadsToIngest(
      filteredLeads,
      config.flowConfigurations.otherLeadsToIngest,
    );

    // Total leads to be ingested
    const leadsToIngest = leadsToUpdate.concat(otherLeadsToIngest);

    // Total leads to be included in the CSV file
    // this step usually orders the array and the make each lead unique by a law
    const leadsToFile = this.getLeadsForFile(
      leadsToIngest,
      config.flowConfigurations.csvContent.beforeColumnsSelection,
    );

    // Extract values from the leads and build the CSV file
    const csv = this.pickValuesFromLeads(
      leadsToFile,
      config.flowConfigurations.csvContent.columns,
    );

    // Generate CSV like file
    const CSVLeads = this.formatCSV(
      csv,
      config.flowConfigurations.csvContent.columns,
    );

    return { CSVLeads, leadsToUpdate };
  }

  protected getCommonData(
    academicPartnerId: string,
    config?: ISendLeadsCSVIntegrationConfigs,
    leadPeriod?: ILeadsTimeframe,
  ): ILeadsWithMetadata[] {
    let results: ILeadsWithMetadata[];
    // Get Leads
    const leads = this.coreService.getLeadsByAP(academicPartnerId, {
      includeConcent: config.flowConfigurations.fetchingData.consents !== false,
      from: leadPeriod.from,
      to: leadPeriod.to,
    });

    results = leads.map<ILeadsWithMetadata>((lead: LeadWithConsent) => {
      if (!lead.consent) return { lead, metadata: {} };

      const { consent } = lead;
      return { lead: _.omit(lead, ['consent']), metadata: { consent } };
    });

    // Get employees
    if (config.flowConfigurations.fetchingData.employees !== false) {
      const employeesIds = [
        ...new Set(
          leads
            .map((lead) => lead.instrideEmployeeId)
            .filter((instrideEmployeeId) => !!instrideEmployeeId),
        ),
      ];

      const employees: Employee[] =
        this.coreService.getEmployeesByIds(employeesIds);

      results = results.map((result) => {
        const foundEmployee = employees.find(
          (employee) => employee.id === result.lead.instrideEmployeeId,
        );

        if (!foundEmployee) return result;

        return {
          ...result,
          metadata: {
            ...result.metadata,
            employee: foundEmployee,
          },
        };
      });
    }

    // Get Corporate Partners
    if (config.flowConfigurations.fetchingData.corporatePartners !== false) {
      const corporatePartnerIds = [
        ...new Set(
          leads
            .map((lead) => lead.corporatePartnerId)
            .filter((corporatePartnerId) => !!corporatePartnerId),
        ),
      ];

      const corporatePartners: CorporatePartner[] =
        this.coreService.getCorporatePartnersByIds(corporatePartnerIds);

      results = results.map((result) => {
        const foundCP = corporatePartners.find(
          (cp) => cp.id === result.lead.instrideEmployeeId,
        );

        if (!foundCP) return result;

        return {
          ...result,
          metadata: {
            ...result.metadata,
            corporatePartner: foundCP,
          },
        };
      });
    }

    // Get Programs
    if (config.flowConfigurations.fetchingData.programs !== false) {
      const programIds = [
        ...new Set(
          leads
            .map((lead) => lead.programKey)
            .filter((programKey) => !!programKey),
        ),
      ];

      const programs: Program[] = this.coreService.getProgramsByIds(programIds);

      results = results.map((result) => {
        const foundProgram = programs.find(
          (program) => program.externalId === result.lead.programKey,
        );

        if (!foundProgram) return result;

        return {
          ...result,
          metadata: {
            ...result.metadata,
            program: foundProgram,
          },
        };
      });
    }

    // Get Profiles
    if (config.flowConfigurations.fetchingData.profiles !== false) {
      const leadsWithOutProfile: Lead[] = [];
      const profileIdsFromLeads = [
        ...new Set(
          leads
            .map((lead) => {
              if (!lead.profileId) leadsWithOutProfile.push(lead);

              return lead.profileId;
            })
            .filter((profileId) => !!profileId),
        ),
      ];

      const profilesFromLeads: Profile[] =
        this.coreService.getProfilesByIds(profileIdsFromLeads);

      const profileIdsFromEmployees = [
        ...new Set(
          leadsWithOutProfile
            .map((lead) => lead.profileId)
            .filter((profileId) => !!profileId),
        ),
      ];

      const profilesFromEmployees: Profile[] =
        this.coreService.getProfilesByIds(profileIdsFromEmployees);

      results = results.map((result) => {
        let foundProfile = profilesFromLeads.find(
          (profile) => profile.id === result.lead.profileId,
        );

        if (!foundProfile) {
          foundProfile = profilesFromEmployees.find(
            (profile) => profile.id === result.lead.profileId,
          );

          if (!foundProfile) return result;
        }

        const isRecipient = !!foundProfile?.profileType.find(
          (type) => type.role === 'RECIPIENT',
        );

        return {
          ...result,
          metadata: {
            ...result.metadata,
            profile: foundProfile,
            isRecipient,
          },
        };
      });
    }

    return results;
  }

  protected filterData(
    leadsWithMetadata: ILeadsWithMetadata[],
    criteria?: ISendLeadsCSVIntegrationConfigs,
  ): ILeadsWithMetadata[] {
    const {
      omitTestUsers = false,
      flowConfigurations: { filteringCritaria = {} },
    } = criteria;

    return leadsWithMetadata.filter((leadWithMetada) => {
      const { corporatePartners, employees, leads, profiles } =
        filteringCritaria;

      if (corporatePartners) {
        const { omitIds } = corporatePartners;

        const shouldExclude = omitIds.some(
          (cpid) => cpid === leadWithMetada.metadata.corporatePartner.id,
        );

        if (shouldExclude) return false;
      }

      // TODO REMOVE LEADS THAT DOESNT HAVE CONSENT
      // TODO REMOVE LEADS THAT DOESN'T HAVE INSTRIDE EMPLOYEEID
      // TODO EXLUCE LEADS FROM CPS
      // TODO OMIT TEST USERS omitTestUsers

      // Filter lead, employee,profile,cp,consent here
      return true;
    });
  }

  protected getLeadsToUpdate(
    filteredLeads: ILeadsWithMetadata[],
    leadsToUpdate: IFlowConfigurations['leadsToUpdate'],
  ): ILeadsWithMetadata[] {
    return filteredLeads.filter((leadsWithMetadata) =>
      leadsToUpdate.some(
        ({
          corporatePartners = {},
          employees = {},
          leads = {},
          programs = {},
        }) => {
          const { eligibilityStatus } = employees;

          // TODO REFACTOR
          const employeeCriteria =
            (eligibilityStatus?.include.includes(
              leadsWithMetadata.metadata?.employee.eligibilityStatus,
            ) ??
              true) &&
            (!eligibilityStatus?.exclude.includes(
              leadsWithMetadata.metadata?.employee.eligibilityStatus,
            ) ??
              true);

          // TODO complete criterias
          const leadsCriteria = true;
          const programsCriteria = true;
          const corporatePartnersCriteria = true;

          return (
            employeeCriteria &&
            leadsCriteria &&
            programsCriteria &&
            corporatePartnersCriteria
          );
        },
      ),
    );
  }

  protected getOtherLeadsToIngest(
    filteredLeads: ILeadsWithMetadata[],
    configs: IFlowConfigurations['otherLeadsToIngest'] = [],
  ): ILeadsWithMetadata[] {
    return configs.length ? this.getLeadsToUpdate(filteredLeads, configs) : [];
  }

  protected getLeadsForFile(
    leadsToIngest: ILeadsWithMetadata[],
    beforeColumnsSelection: ICSVContentSteps[] = [],
  ): ILeadsWithMetadata[] {
    const orderedSteps = beforeColumnsSelection.sort(
      (criteria1, criteria2) => criteria1.order - criteria2.order,
    );

    for (const {
      method: { name, args },
    } of orderedSteps) {
      leadsToIngest = this.utilsService.applyMethod(name, args, leadsToIngest);
    }

    return leadsToIngest;
  }

  protected pickValuesFromLeads(
    leadsWithMetadata: ILeadsWithMetadata[],
    pickingCriteria: ICSVContent['columns'] = [],
  ): PickedValues[] {
    const pickValues = (leadWithMetadata: ILeadsWithMetadata) =>
      pickingCriteria.reduce(
        (acc, { columnName, applyFunction, fallback, fixedValue, sources }) => {
          if (fixedValue) return { ...acc, [columnName]: String(fixedValue) };

          let value: unknown;

          for (let i = 0; i < sources.length; i++) {
            const source = sources[i];
            value = _.get(leadWithMetadata, source);
            if (value !== undefined) break;
            if (i === sources.length) value = fallback ?? '';
          }

          if (applyFunction)
            value = this.utilsService.parseValue(value, applyFunction);

          return { ...acc, [columnName]: String(value) };
        },
        {},
      );

    return leadsWithMetadata.map(pickValues);
  }

  protected formatCSV(
    pickedValues: PickedValues[],
    coulmns: ICSVContent['columns'] = [],
  ): ICSVFlowResult['CSVLeads'] {
    if (coulmns.length === 0) return '';

    const orderedColumns = coulmns.sort((c1, c2) => c1.order - c2.order);

    const header = orderedColumns.map(({ columnName }) => columnName);

    const body = pickedValues.map((values) =>
      header.map((columnName) => values[columnName]).join(','),
    );

    return [header.join(','), ...body].join('\n');
  }
}
