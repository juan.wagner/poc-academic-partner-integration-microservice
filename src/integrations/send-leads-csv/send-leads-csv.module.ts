import { Module } from '@nestjs/common';
import { AcademicPartnerCommunicationModule } from '../../shared/academic-partner-communication/academic-partner-communication.module';
import { CoreapiModule } from '../../shared/coreapi/coreapi.module';
import { DefaultFlow } from './flows/DefaultFlow';
import { LeadTimeframe } from './utils/LeadTimeframe';
import { SendLeadsCsvRepository } from './send-leads-csv.repository';
import { SendLeadsCsvService } from './send-leads-csv.service';
import { FlowUtils } from './utils/FlowUtils';

@Module({
  providers: [
    SendLeadsCsvService,
    DefaultFlow,
    FlowUtils,
    SendLeadsCsvRepository,
    LeadTimeframe,
  ],
  imports: [CoreapiModule, AcademicPartnerCommunicationModule],
  exports: [SendLeadsCsvService],
})
export class SendLeadsCsvModule {}
