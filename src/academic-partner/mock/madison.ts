// One AP with two ways of performing the same integration. We differentiate them by their name
// const madison = new AP({
//   name: 'madison',
//   ferpaEnabled: true,
//   id: 'ID',
//   serviceName: 'madison',
//   mappedCorporatePartners: [],
//   integrations: [
//     {
//       type: AcademicPartnerIntegration.CREATE_LEAD_CSV,
//       name: 'credit',
//       config: {
//         flow: CreateLeadsCSVFlow.MADINSON,
//         timeFrame: { type: TimeFrameType.CUMMULATIVE },
//         updateProcesedLeadsStatus: false,
//         communication: { type: CommunicationMethod.SFTP, config: {} },
//         groupingCriteria: {
//           leadKeys: ['profileId', 'instrideEmployeeId'],
//         },
//         csvContent: [{ columnName: '' }],
//       },
//     },
//     {
//       type: AcademicPartnerIntegration.CREATE_LEAD_CSV,
//       name: 'non-credit',
//       config: {
//         flow: CreateLeadsCSVFlow.MADINSON,
//         timeFrame: { type: TimeFrameType.DELTA, config: { days: 1 } },
//         updateProcesedLeadsStatus: true,
//         communication: { type: CommunicationMethod.SFTP, config: {} },
//         csvContent: [{ columnName: '' }],
//       },
//     },
//   ],
// });