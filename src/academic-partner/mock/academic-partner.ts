import {
  IAcademicPartner,
  IMappedCorporatePartners,
  IProgramPageFields,
} from '../types/academic-partner.types';
import { IIntegrations } from '../types/integrations.types';

export class AP implements IAcademicPartner {
  ferpaEnabled: boolean;
  id: string;
  mappedCorporatePartners: IMappedCorporatePartners[];
  name: string;
  opeId?: string;
  programPageFields?: IProgramPageFields[];
  redirectURL?: string;
  serviceName: string;
  shortName?: string;
  termsAndConditionsApproval?: boolean;
  integrations: IIntegrations[];

  constructor(init: IAcademicPartner) {
    for (const key in init) {
      if (Object.prototype.hasOwnProperty.call(init, key)) {
        this[key] = init[key];
      }
    }
  }
}
