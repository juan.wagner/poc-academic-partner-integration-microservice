import {
  CreateLeadsCSVFlow,
  TimeFrameType,
} from '../../integrations/send-leads-csv/types/SendLeadsCSV.integration.type';
import { CommunicationMethod } from '../../shared/academic-partner-communication/types/academic-partner-communication.types';
import { AcademicPartnerIntegration } from '../types/integrations.types';
import { AP } from './academic-partner';

export const cuny = new AP({
  name: 'cuny',
  ferpaEnabled: true,
  id: 'ID',
  serviceName: 'cuny',
  mappedCorporatePartners: [],
  // Denver Scope ⬇
  integrations: [
    {
      // CSV FILE INTEGRATION ⬇
      type: AcademicPartnerIntegration.CREATE_LEAD_CSV,
      config: {
        flow: CreateLeadsCSVFlow.NORMAL_FLOW,
        leadsTimeframe: { type: TimeFrameType.CUMMULATIVE },
        communication: { type: CommunicationMethod.SFTP, config: {} },
        omitTestUsers: true,
        dryRun: false,
        flowConfigurations: {
          fetchingData: {
            employees: true,
            profiles: true,
            corporatePartners: true,
          },
          filteringCritaria: {
            corporatePartners: { omitIds: ['M&S_ID'] },
          },
          leadsToUpdate: [
            {
              employees: { eligibilityStatus: { include: ['ELIGIBLES'] } },
              leads: {
                leadStatus: {
                  include: [
                    'SCHEDULED',
                    'RECEVIED',
                    'SENT',
                    'PENDING_NOTIFICATION',
                  ],
                },
              },
            },
            {
              employees: { eligibilityStatus: { include: ['ELIGIBLES'] } },
              leads: {
                leadStatus: {
                  include: ['PENDING'],
                },
              },
              corporatePartners: {
                flags: {
                  exclude: [
                    'holdLeads',
                    'employeeApproval',
                    'managerApproval',
                    'managerApproval2',
                  ],
                },
              },
            },
          ],
          otherLeadsToIngest: [
            {
              employees: { eligibilityStatus: { exclude: ['ELIGIBLES'] } },
              leads: {
                leadStatus: { include: ['SCHEDULED', 'RECEVIED', 'SENT'] },
              },
            },
          ],
          csvContent: {
            beforeColumnsSelection: [
              {
                order: 0,
                method: {
                  name: 'orderBy',
                  args: [['lead.leadDate'], ['desc']],
                },
              },
              {
                order: 1,
                method: {
                  name: 'uniqWith',
                  args: ['lead.instrideEmployeeId', 'metadata.isRecipient'],
                },
              },
            ],
            columns: [
              {
                order: 0,
                columnName: 'instrideId',
                sources: ['lead.instrideEmployeeId'],
              },
              {
                order: 1,
                columnName: 'corporatePartnerName',
                sources: ['metadata.corporatePartner.name'],
              },
              {
                order: 2,
                columnName: 'firstName',
                sources: ['lead.firstName'],
              },
              {
                order: 3,
                columnName: 'lastName',
                sources: ['lead.lastName'],
              },
              {
                order: 4,
                columnName: 'email',
                sources: ['lead.email'],
              },
              {
                order: 5,
                columnName: 'phone',
                sources: ['lead.phone'],
              },
              {
                order: 6,
                columnName: 'dateOfBirth',
                sources: ['lead.dateOfBirth'],
                applyFunction: 'dateFormatter',
              },
              {
                order: 7,
                columnName: 'programId',
                sources: ['lead.programKey'],
              },
              {
                order: 8,
                columnName: 'eligible',
                sources: ['metadata.employee.eligibilityStatus'],
                applyFunction: 'isEligible',
              },
              {
                order: 9,
                columnName: 'date',
                sources: ['lead.leadDate'],
                applyFunction: 'dateFormatter',
              },
              {
                order: 10,
                columnName: 'scholarshipPercent',
                sources: [
                  'metadata.corporatePartner.configuration.scholarship',
                ],
                fallback: 0,
              },
              {
                order: 11,
                columnName: 'benefitNotes',
                sources: [
                  'metadata.corporatePartner.configuration.benefitNotes',
                ],
              },
              {
                order: 12,
                columnName: 'directBill',
                sources: ['metadata.corporatePartner.configuration.directBill'],
                applyFunction: 'normalizeDirectBill',
              },
              {
                order: 12,
                columnName: 'termsAndConditions',
                fixedValue: 'Y',
              },
              {
                order: 13,
                columnName: 'isRecipient',
                sources: ['metadata.isRecipient'],
                fallback: 'N',
              },
              {
                order: 14,
                columnName: 'country',
                sources: ['metadata.employee.dynamicFields.country'],
                applyFunction: 'parseCountry',
                fallback: 'USA',
              },
              {
                order: 15,
                columnName: 'dateEligibilityLost',
                sources: ['metadata.employee.eligibilityStatus'],
                applyFunction: 'parseEligibilityStatus',
              },
            ],
          },
        },
      },
    },
  ],
});
