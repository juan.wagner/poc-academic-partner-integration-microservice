import { Body, Controller, Param, Post } from '@nestjs/common';
import { AcademicPartnerService } from './academic-partner.service';
import { IAcademicPartner } from './types/academic-partner.types';
import { cuny } from './mock/cuny';

@Controller('academic-partner')
export class AcademicPartnerController {
  constructor(private readonly service: AcademicPartnerService) {}

  @Post('sendLeadCSV/:academicPartnerId')
  sendLeadsCSV(
    @Param('academicPartnerId') academicPartnerId: string,
    // multiples integration are identified by name ex. madison credit non-credit
    @Body('name') name?: string,
  ) {
    // const ap: IAcademicPartner =
    // this.coreAPIService.getAcademicPartnerdById(academicPartnerId);
    const ap: IAcademicPartner = cuny;

    const result = this.service.sendLeadsCSV(ap, name);

    // returns the resul of the integration
    return { result, ...(name && { integrationName: name }) };
  }
}
