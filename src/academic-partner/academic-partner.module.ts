import { Module } from '@nestjs/common';
import { AcademicPartnerService } from './academic-partner.service';
import { SendLeadsCsvModule } from '../integrations/send-leads-csv/send-leads-csv.module';
import { AcademicPartnerController } from './academic-partner.controller';
import { CoreapiModule } from '../shared/coreapi/coreapi.module';

@Module({
  providers: [AcademicPartnerService],
  imports: [SendLeadsCsvModule, CoreapiModule],
  controllers: [AcademicPartnerController],
})
export class AcademicPartnerModule {}
