import { BadRequestException, Injectable } from '@nestjs/common';
import { SendLeadsCsvService } from '../integrations/send-leads-csv/send-leads-csv.service';
import { IAcademicPartner } from './types/academic-partner.types';
import { AcademicPartnerIntegration } from './types/integrations.types';

@Injectable()
export class AcademicPartnerService {
  constructor(private readonly sendLeadsCSVService: SendLeadsCsvService) {}

  sendLeadsCSV(ap: IAcademicPartner, integrationName: string) {
    // check integration exists for AP
    const integrations = ap.integrations.filter(
      (integration) =>
        integration.type === AcademicPartnerIntegration.CREATE_LEAD_CSV,
    );

    const integration = integrationName
      ? integrations.find((integration) => integration.name === integrationName)
      : integrations?.[0];

    if (
      !integration ||
      // Added for TS safety...
      integration.type !== AcademicPartnerIntegration.CREATE_LEAD_CSV
    )
      throw new BadRequestException('No integration found!');

    if (integration.disabled)
      throw new BadRequestException('Integration is disabled!');

    const result = this.sendLeadsCSVService.sendLeadsCSV(ap.id, integration);

    return { integration, result };
  }
}
