import { IDirectIntegrationIntegration } from '../../integrations/send-leads-direct-integration/types/SendLeadDirectIntegration.integration.type';
import type { ISendLeadsCSVIntegration } from '../../integrations/send-leads-csv/types/SendLeadsCSV.integration.type';

// These all the different integration that we have nowadays witht the AP
export enum AcademicPartnerIntegration {
  // we send leads by creating a CSV file and uploading to a SFTP
  CREATE_LEAD_CSV = 'CREATE_LEAD_CSV',
  // We send leads directly with an AP endpoint
  SEND_LEAD_DIRECT_INTEGRATION = 'SEND_LEAD_DIRECT_INTEGRATION',
  // Learner data ingestiong through cp_reports
  INGEST_ENROLLMENT_CSV = 'INGEST_ENROLLMENT_CSV',
  // Learner data but using an endpoint (flexcredit)
  INGEST_ENROLLMENT_ENDPOINT = 'INGEST_ENROLLMENT_ENDPOINT',
  // Create a new enrollment
  ENROLLMENTS = 'ENROLLMENTS',
  // Update enrollment status
  UPDATE_ENROLLMENTS = 'UPDATE_ENROLLMENTS',
  // retrieve enrollments urls
  GET_ENROLLMENTS = 'GET_ENROLLMENTS',
  // send employees' eligibility (ASU)
  SEND_ELIGIBILITY = 'SEND_ELIGIBILITY',
  // Report
  BILLING_REPORT = 'BILLING_REPORT',
  // related to send eligibility but in bulk (ASU)
  BATCH_PROCESS = 'BATCH_PROCESS',
  // Events triggered when retro consent is signed
  RETRO_CONSENT = 'RETRO_CONSENT',
  // real integration or internal tool?
  RETRY_QUEUE = 'RETRY_QUEUE',
}

// Common interface for every integration
export interface IIntegrationBase {
  // We can select what integration we are configurating
  type: AcademicPartnerIntegration;
  // integration name in case of an AP perform the same integration but in different ways (ex. madison - send CSV leads -> credit / non-credit)
  name?: string;
  // defines if the integration is enables / disabled
  disabled?: boolean;
  // integration config
  config?: IIntegrationConfigBase;
}

// shared capabilities of any integration
export interface IIntegrationConfigBase {
  // neither send anything to AP nor write anything in DB
  dryRun?: boolean;
  // ignore test users (embtest) in the final result of any integration
  omitTestUsers?: boolean;
}

// We can enforce type safety
export type IIntegrations =
  // Defined interface for sending leads by CSV
  | ISendLeadsCSVIntegration
  // Defined interface for sending leads by direct integration
  | IDirectIntegrationIntegration;
