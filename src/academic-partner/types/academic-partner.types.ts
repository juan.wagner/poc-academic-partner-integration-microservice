import type { IIntegrations } from './integrations.types';

export interface IMappedCorporatePartners {
  canonicalName: string;
  dependentSubclass?: string;
  externalId: string;
  secondaryExternalId: string;
  corporatePartnerId: string;
  configuration: {
    eligibilityReport: EligibilityReport;
  };
}

enum EligibilityReport {
  ALL_EMPLOYEES = 'ALL_EMPLOYEES',
  LEAD_MAPPED_EMPLOYEES = 'LEAD_MAPPED_EMPLOYEES',
}

export interface IProgramPageFields {
  key: string;
  order: number;
}
// The academic partner interface as is set in core API
export interface IAcademicPartner {
  ferpaEnabled: boolean;
  id: string;
  mappedCorporatePartners: IMappedCorporatePartners[];
  name: string;
  opeId?: string;
  programPageFields?: IProgramPageFields[];
  redirectURL?: string;
  serviceName: string;
  shortName?: string;
  termsAndConditionsApproval?: boolean;
  // Group all the different integrations that we can have with the APs in a single array of integrations.
  integrations: IIntegrations[];
}
