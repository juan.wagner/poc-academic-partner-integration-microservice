import { Injectable } from '@nestjs/common';
import { ILeadsWithMetadata } from '../../integrations/send-leads-csv/types/CSVFlow.types';
import { IAcademicPartner } from '../../academic-partner/types/academic-partner.types';
import {
  CorporatePartner,
  Employee,
  Lead,
  LeadWithConsent,
  Profile,
  Program,
} from './types/generic.types';

@Injectable()
export class CoreapiService {
  updateLeads(leadsToUpdate: ILeadsWithMetadata[]) {
    throw new Error('Method not implemented.');
  }
  getLeadsByAP(
    academicPartnerId: string,
    getLeadsConfigs: { from?: Date; to?: Date; includeConcent: boolean },
  ): LeadWithConsent[] | Lead[] {
    const { from, to, includeConcent } = getLeadsConfigs;

    const requestOptions = {
      // Default, take consent from leads
      ...(includeConcent && {
        expand: 'consent',
      }),
      ...(!!to && { to }),
      ...(!!from && { from }),
    };

    // return leads as Lead[];
    throw new Error('Method not implemented.');
  }
  getAcademicPartnerdById(academicPartnerId: string): IAcademicPartner {
    throw new Error('Method not implemented.');
  }
  getCorporatePartnersByIds(
    corporatePartnersIds: string[],
  ): CorporatePartner[] {
    throw new Error('Method not implemented.');
  }
  getProgramsByIds(programsExternalIds: string[]): Program[] {
    throw new Error('Method not implemented.');
  }
  getEmployeesByIds(employeesIds: string[]): Employee[] {
    throw new Error('Method not implemented.');
  }
  getProfilesByIds(profileIds: string[]): Profile[] {
    throw new Error('Method not implemented.');
  }
}
