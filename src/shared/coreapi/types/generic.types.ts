export type CorporatePartner = {
  address: {
    street: 'Street';
    city: 'City';
    country: 'Country';
    zip: 'Zip';
    state: 'CA';
  };
  configuration: {
    academicPartners: [
      {
        id: 'd8ca23ba-d5b9-49b2-9ab8-4f65f6a8a2d2';
        programPageConfig: {
          cta: {
            title: 'You may take up to 3 FlexCredit courses at a time.';
            text: 'Enroll Now';
          };
        };
      },
      {
        id: 'beb42643-f127-4ae3-8968-c46fe8ebad10';
        programPageConfig: {
          cta: {
            title: 'You may take up to 3 FlexCredit courses at a time.';
            text: 'Enroll Now';
          };
        };
      },
      {
        id: '25ce942d-8480-4846-86e1-ab315fafa94b';
        programPageConfig: {
          cta: {
            text: 'Enroll Now';
            title: 'You may take up to 3 FlexCredit courses at a time.';
          };
        };
      },
      {
        id: '3a07f97e-650f-4a59-9eb3-685d73ddac38';
        programPageConfig: {
          cta: {
            text: 'Enroll Now';
          };
        };
      },
    ];
    eligibilityRules: [
      {
        name: 'Eligibility';
        conditions: {
          all: [
            {
              fact: 'employmentStatus';
              params: {
                message: 'Must be a full-time Main and Stern employee';
              };
              value: 'F';
              operator: 'equal';
            },
            {
              fact: 'hireDate';
              params: {
                message: 'Must have 12 months of service with Main and Stern';
              };
              value: '1y';
              operator: 'moreThanInTime';
            },
          ];
        };
        event: {
          params: {
            value: 'ELIGIBLE';
          };
          type: 'eligible';
        };
        priority: 1;
      },
    ];
    futureEligible: false;
    pilotMode: false;
    reimbursementEnabled: true;
    auth0Connections: [
      {
        connection: 'Learner-Database-Connection';
        provider: 'auth0';
      },
    ];
    showArticleTags: ['all', 'asu', 'Main & Stern'];
    hideArticleTags: ['none'];
    cap: {
      currency: 'USD';
      value: 500;
    };
    enrollmentLimitRule: {
      rule: 'EnrollmentsPerLearner';
      value: 3;
    };
    fileFields: {
      firstName: {
        '@type': 'string';
        required: true;
        max: 50;
        tokenized: true;
      };
      lastName: {
        '@type': 'string';
        required: true;
        max: 50;
        tokenized: true;
      };
      hireDate: {
        '@type': 'date';
        required: true;
        tokenized: false;
      };
      dateOfBirth: {
        '@type': 'date';
        required: true;
        tokenized: true;
      };
      employeeId: {
        '@type': 'string';
        required: true;
        max: 256;
        tokenized: true;
      };
      employmentStatus: {
        '@type': 'string';
        required: true;
        max: 30;
        tokenized: false;
      };
      email: {
        '@type': 'email';
        required: true;
        max: 256;
        tokenized: true;
      };
      managerEmail: {
        '@type': 'email';
        max: 256;
        tokenized: true;
      };
    };
    headerMapFields: {
      firstName: 'firstName';
      lastName: 'lastName';
      hireDate: 'hireDate';
      dateOfBirth: 'dateOfBirth';
      employeeId: 'employeeId';
      employmentStatus: 'employmentStatus';
      email: 'email';
      managerEmail: 'managerEmail';
      eligible: 'eligible';
    };
    hrContact: 'ncinc@mailto.com';
    id: '5bf3a9ca-8b17-4d8f-9c32-e5b1bc032284';
    jobListingUrl: 'https://jobs.lever.co/instride';
    name: 'Main and Stern';
    registrationSupported: true;
    liveLearnerSupport: {
      enabled: true;
      phoneNumber: '1-800-555-1234';
    };
    uniqueIdentifier: {
      employeeId: {
        '@type': 'string';
        required: true;
        max: 256;
      };
    };
    checkForEligibility: true;
    groupsEnabled: false;
    programAvailabilitiesEnabled: false;
  };
  _id: '5ec5852be7638372cefb77f3';
  canonicalName: 'mainandstern';
  eligibilityCheck: true;
  dynamicFlow: true;
  id: '5bf3a9ca-8b17-4d8f-9c32-e5b1bc032284';
  name: 'Orange Corp';
  fullFileFlow: true;
  manualVerification: false;
  createdAt: '2020-01-01T00:00:00.000Z';
  creator: 'Admin';
};

export type Consent = {
  status: 'AGREED';
  history: [
    {
      status: 'AGREED';
      updatedAt: string;
    },
  ];
  _id: string;
  email: string;
  targetId: string;
  profileId: string;
  signature: string;
  createdAt: string;
  updatedAt: string;
  signee: object;
};

export type Program = {
  _id: string;
  tags: [];
  skillTags: [];
  interestAreas: ['Criminal Justice & Law'];
  instructors: [];
  source: string;
  syncedAt: string;
  academicPartner: {
    id: string;
    name: string;
    shortName: string;
  };
  careers: {
    description: string;
    items: [
      {
        title: string;
      },
    ];
  };
  credits: number;
  curriculum: {
    description: string;
    items: [
      {
        description: string;
        title: string;
      },
    ];
  };
  school: {
    description: string;
    items: [];
  };
  externalId: 'GRPP-PPCRIMJMA';
  requirements: {
    description: string;
    items: {
      description: string;
      title: string;
    }[];
  };
  shortDescription: string;
  image: string;
  longDescription: {
    description: string;
    items: [];
  };
  prerequisites: string;
  title: string;
  type: 'Degrees';
  subType: 'Graduate Degrees';
  startDate: string;
  totalClasses: number;
  duration: {
    count: '7.5';
    units: 'WEEKS';
  };
  price: '0';
  updatedAt: string;
  enrollType: 'LEAD_ENROLLMENT';
  path: string;
  overviewDescription: '';
  learningSubType: 'Graduate Degrees';
  learningType: 'Graduate Studies';
  createdAt: '2021-09-10T21:53:42.794Z';
  timeToComplete: '1.5 - 3.5 years';
};

export type Employee = {
  _id: string;
  corporatePartnerId: string;
  crmSystem: string;
  dynamicFields: {
    firstName: string;
    lastName: string;
    employeeId: string;
    eligible: string;
    division: string;
    employmentStatus: string;
    managerEmail: string;
    managerFirstName: string;
    hrEmail: string;
    hrContactName: string;
    hireDate: string;
  };
  source: string;
  uniqueIdentifier: string;
  eligibilityStatus: string;
  statusDate: string;
  id: string;
  createdAt: string;
  eligibilityHistory: [];
  indexFields: {
    firstName: string;
    lastName: string;
  };
  terminated: {
    createdAt: string;
    history: [];
    status: false;
    updatedAt: string;
  };
  updatedAt: string;
  profileId?: string;
  recipients: [
    {
      _id: string;
      id: string;
      corporatePartnerId: string;
      createdAt: string;
      firstName: string;
      indexFields: {
        firstName: string;
        lastName: string;
      };
      instrideEmployeeId: string;
      lastName: string;
      profileId: string;
      relationship: 'Parent';
      status: 'RECIPIENT_ACCOUNT_PENDING';
      updatedAt: string;
    },
  ];
};

export type Lead = Omit<LeadWithConsent, 'consent'>;

export type LeadWithConsent = {
  indexFields: {
    email: string;
    firstName: string;
    lastName: string;
    dynamicFields: {
      otherEmail: string;
    };
  };
  _id: string;
  clientId: string;
  conversationId: string;
  corporatePartnerId: string;
  dateOfBirth: string | number;
  degreeType: 'Degrees';
  educationPartnerId: string;
  email: string;
  enterpriseClientId: string;
  firstName: string;
  lastName: string;
  leadStatus: string;
  leadType: 'RFI';
  originURI: 'https://labcorp.latest.instride.com/rfi';
  phone: string;
  programKey: string;
  programName: string;
  dynamicFields: {
    otherEmail: string;
  };
  createdAt: string;
  id: string;
  leadDate: string | number;
  leadSource: string;
  updatedAt: string;
  __v: 0;
  instrideEmployeeId: string;
  profileId?: string;
  consent: {
    status: 'AGREED';
    history: [
      {
        status: 'AGREED';
        updatedAt: string;
      },
    ];
    _id: string;
    email: string;
    targetId: string;
    profileId: string;
    signature: string;
    createdAt: string;
    updatedAt: string;
    signee: object;
  };
};

export type Profile = {
  indexFields: {
    firstName: string;
    lastName: string;
    email: string;
  };
  _id: '61002842360c6902ffed217a';
  email: string;
  externalId: string;
  progressTowardsEducation: 'ACCOUNT_COMPLETED';
  firstName: string;
  lastName: string;
  id: 'f4eb9fbc-5740-4190-81bc-a55b3b9d4d20';
  createdAt: string;
  updatedAt: string;
  pathFinder: {
    question: string;
    response: string;
  };
  auth0UserId: string;
  originURI: string;
  address: object;
  profileType: [
    {
      role: 'RECIPIENT';
      _id: '610028447f9a129aafb5a657';
      id: 'b66bfef6-7e68-426b-b9a5-967a28b824a8';
      corporatePartnerId: '5bf3a9ca-8b17-4d8f-9c32-e5b1bc032284';
      createdAt: '2021-07-27T15:37:39.950Z';
      firstName: 'embtest';
      indexFields: {
        firstName: 'embtest';
        lastName: 'instride';
      };
      instrideEmployeeId: '1b970ff1-6645-4088-a6a0-8941217ee2e6';
      lastName: 'instride';
      profileId: 'f4eb9fbc-5740-4190-81bc-a55b3b9d4d20';
      relationship: 'Sibling';
      status: 'RECIPIENT_AC1_CREATED';
      updatedAt: '2021-07-27T19:51:24.507Z';
    },
  ];
  hasEducationAccess: boolean;
  hasReimbursementEducationAccess: boolean;
  reimbursementData: {
    annualMaximum: number;
    remaining: number;
  };
};
