import { Module } from '@nestjs/common';
import { CoreapiService } from './coreapi.service';

@Module({
  providers: [CoreapiService],
  exports: [CoreapiService],
})
export class CoreapiModule {}
