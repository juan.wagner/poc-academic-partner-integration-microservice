import { Injectable } from '@nestjs/common';
import { ICSVFlowResult } from '../../integrations/send-leads-csv/types/CSVFlow.types';
import {
  CommunicationMethod,
  IAPCommunication,
} from './types/academic-partner-communication.types';

@Injectable()
export class AcademicPartnerCommunicationService {
  sendCSV(
    CSVLeads: ICSVFlowResult['CSVLeads'],
    communication: IAPCommunication,
  ) {
    if (communication.type === CommunicationMethod.SFTP) {
      // Upload to SFTP
    }

    if (communication.type === CommunicationMethod.ENDPOINT) {
      // Request an endpoint
    }
  }
}
