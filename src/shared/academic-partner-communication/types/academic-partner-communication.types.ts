export enum CommunicationMethod {
  SFTP = 'SFTP',
  ENDPOINT = 'ENDPOINT', // Comming Westminster
  SHAREPOINT = 'SHAREPOINT', // HSBO?
}

export interface IAPCommunication {
  type: CommunicationMethod;
  config: Record<string, unknown>;
}
