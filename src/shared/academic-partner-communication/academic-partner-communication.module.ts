import { Module } from '@nestjs/common';
import { AcademicPartnerCommunicationService } from './academic-partner-communication.service';

@Module({
  providers: [AcademicPartnerCommunicationService],
  exports: [AcademicPartnerCommunicationService],
})
export class AcademicPartnerCommunicationModule {}
