import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AcademicPartnerModule } from './academic-partner/academic-partner.module';

@Module({
  imports: [AcademicPartnerModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
